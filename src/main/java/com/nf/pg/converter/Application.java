package com.nf.pg.converter;

import com.google.common.base.Preconditions;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.nf.pg.converter.exceptions.UnitConverterOperationException;
import com.nf.pg.converter.parser.implementation.FileDataProvider;
import com.nf.pg.converter.parser.implementation.FileDataWriter;
import com.nf.pg.converter.parser.implementation.LogDataWriter;
import com.nf.pg.converter.utils.UnitConverterModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;

/**
 * Converter application main class.
 */
public final class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    @Inject
    private UnitConverter converter;

    /**
     * Application entry point.
     *
     * @param args Input parameters. File paths is acceptable values.
     */
    public static void main(String[] args) {
        Application application = createApplication();
        application.start(args);
    }

    @Nonnull
    private static Application createApplication() {
        Injector injector = Guice.createInjector(new UnitConverterModule());
        return injector.getInstance(Application.class);
    }

    private void start(@Nonnull String[] paths) {
        try {
            checkStartupParameters(paths);
            getConverter().registerResultWriters(new FileDataWriter("output.txt"), new LogDataWriter());
            getConverter().load(new FileDataProvider(paths[0]));
        } catch (UnitConverterOperationException e) {
            log.error("Cannot load data for converting", e);
        }
    }

    private void checkStartupParameters(@Nonnull String[] args) throws UnitConverterOperationException {
        if (args.length == 0) {
            throw new UnitConverterOperationException("No files specified");
        }
    }

    @Nonnull
    private UnitConverter getConverter() {
        return this.converter;
    }
}
