package com.nf.pg.converter.exceptions;

/**
 * Represents unit assignment pair parsing error.
 * @see com.nf.pg.converter.entities.UnitAssignment
 */
public class UnitAssignmentParsingException extends Exception {

    public UnitAssignmentParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
