package com.nf.pg.converter.exceptions;

/**
 * Represents common error during the unit conversion process.
 */
public class UnitConverterOperationException extends Exception {

    public UnitConverterOperationException(String message) {
        super(message);
    }

    public UnitConverterOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
