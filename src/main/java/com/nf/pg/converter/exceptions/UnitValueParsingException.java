package com.nf.pg.converter.exceptions;

/**
 * Represents error which could occur during the unit value parsing process.
 * @see com.nf.pg.converter.entities.UnitValue
 */
public class UnitValueParsingException extends Exception {

    public UnitValueParsingException(String message) {
        super(message);
    }
}
