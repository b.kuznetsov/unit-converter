package com.nf.pg.converter.parser.api;

import com.nf.pg.converter.entities.UnitAssignment;
import com.nf.pg.converter.exceptions.UnitAssignmentParsingException;

import javax.annotation.Nonnull;

/**
 * Defines single unit assignment parser behavior.
 */
public interface UnitAssignmentParser {

    /**
     * Parses unit assignment line.
     *
     * @param assignment Assignment textual representation.
     * @return Parsed assignment.
     * @throws UnitAssignmentParsingException If specified textual representation has wrong format.
     */
    @Nonnull
    UnitAssignment parse(@Nonnull String assignment) throws UnitAssignmentParsingException;
}
