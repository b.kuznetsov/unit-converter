package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.parser.api.DataProvider;

import javax.annotation.Nonnull;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Reads data from file.
 */
public class FileDataProvider implements DataProvider {

    private String filepath;

    public FileDataProvider(@Nonnull String filepath) {
        this.filepath = filepath;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public InputStream getData() throws IOException {
        return new FileInputStream(filepath);
    }
}
