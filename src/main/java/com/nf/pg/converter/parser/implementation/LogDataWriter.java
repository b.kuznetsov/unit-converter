package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.parser.api.DataWriter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Writes data to log.
 */
public class LogDataWriter implements DataWriter {

    private static final Logger log = LoggerFactory.getLogger(LogDataWriter.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(@Nonnull AssignmentTable data) throws IOException {
        String result = StringUtils.LF + data.toString();
        log.info(result);
    }
}
