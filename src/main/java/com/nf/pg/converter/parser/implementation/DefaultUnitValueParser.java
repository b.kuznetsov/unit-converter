package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.entities.UnitValue;
import com.nf.pg.converter.exceptions.UnitValueParsingException;
import com.nf.pg.converter.parser.api.UnitValueParser;

import javax.annotation.Nonnull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultUnitValueParser implements UnitValueParser {

    private static final Pattern NORMAL_VALUE_PATTERN = Pattern.compile("(^-?\\d*\\.{0,1}\\d+)\\s+(\\w+)$");
    private static final Pattern UNDEFINED_VALUE_PATTERN = Pattern.compile("(^\\?)\\s+(\\w+$)");

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public UnitValue parse(@Nonnull String value) throws UnitValueParsingException {
        String trimmedValue = value.trim();

        Matcher matcher = NORMAL_VALUE_PATTERN.matcher(trimmedValue);
        if (matcher.matches()) {
            return readNormalUnit(matcher);
        }

        matcher = UNDEFINED_VALUE_PATTERN.matcher(trimmedValue);
        if (matcher.matches()) {
            return readUndefinedUnit(matcher);
        }

        throw new UnitValueParsingException(String.format("Cannot parse value [%s]", value));
    }

    @Nonnull
    private UnitValue readNormalUnit(@Nonnull Matcher matcher) {
        Double numericValue = Double.valueOf(matcher.group(1));
        String unitType = matcher.group(2);
        return new UnitValue(unitType, numericValue);
    }

    @Nonnull
    private UnitValue readUndefinedUnit(@Nonnull Matcher matcher) {
        String unitType = matcher.group(2);
        return new UnitValue(unitType);
    }
}
