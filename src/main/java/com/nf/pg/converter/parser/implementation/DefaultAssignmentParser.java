package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.entities.UnitAssignment;
import com.nf.pg.converter.entities.UnitValue;
import com.nf.pg.converter.exceptions.UnitAssignmentParsingException;
import com.nf.pg.converter.exceptions.UnitValueParsingException;
import com.nf.pg.converter.parser.api.UnitAssignmentParser;
import com.nf.pg.converter.parser.api.UnitValueParser;

import javax.annotation.Nonnull;
import javax.inject.Inject;

public class DefaultAssignmentParser implements UnitAssignmentParser {

    private static final String SPLITTER = "=";

    @Inject
    private UnitValueParser valueParser;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public UnitAssignment parse(@Nonnull String assignment) throws UnitAssignmentParsingException {

        String[] parts = assignment.split(SPLITTER);
        if (parts.length != 2) {
            throw new UnsupportedOperationException(String.format("Wrong format of assignment [%s]", assignment));
        }

        String trimmedLeftSide = parts[0].trim();
        String trimmedRightSide = parts[1].trim();

        try {
            UnitValue leftSideValue = valueParser.parse(trimmedLeftSide);
            UnitValue rightSideValue = valueParser.parse(trimmedRightSide);
            return new UnitAssignment(leftSideValue, rightSideValue);
        } catch (UnitValueParsingException e) {
            throw new UnitAssignmentParsingException(String.format("Cannot parse assignment [%s]", assignment), e);
        }
    }
}
