package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.entities.UnitAssignment;
import com.nf.pg.converter.exceptions.UnitAssignmentParsingException;
import com.nf.pg.converter.parser.api.AssignmentTableParser;
import com.nf.pg.converter.parser.api.UnitAssignmentParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class DefaultAssignmentTableParser implements AssignmentTableParser {

    private static final Logger log = LoggerFactory.getLogger(DefaultAssignmentTableParser.class);

    @Inject
    private UnitAssignmentParser assignmentParser;

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public AssignmentTable parse(@Nonnull String data) throws UnitAssignmentParsingException {
        AssignmentTable assignments = new AssignmentTable();
        List<String> lines = Arrays.asList(data.split(StringUtils.LF));
        lines.forEach(line -> processLine(assignments, line));
        return assignments;
    }

    private void processLine(@Nonnull final AssignmentTable table, @Nonnull String line) {
        String trimmedLine = StringUtils.trimToEmpty(line);
        if (trimmedLine.isEmpty()) {
            return;
        }

        try {
            UnitAssignment assignment = assignmentParser.parse(trimmedLine);
            table.addAssignment(assignment);
        } catch (UnitAssignmentParsingException e) {
            log.error("Cannot parse line [" + line + "]", e);
        }
    }
}
