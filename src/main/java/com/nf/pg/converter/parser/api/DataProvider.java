package com.nf.pg.converter.parser.api;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;

/**
 * Defines behavior of system which can load data from somewhere.
 */
public interface DataProvider {

    /**
     * Loads data by standard input streams.
     *
     * @return Reader steam.
     * @throws IOException If something wrong with I/O.
     */
    @Nonnull
    InputStream getData() throws IOException;

}
