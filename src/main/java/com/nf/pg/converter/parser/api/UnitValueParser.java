package com.nf.pg.converter.parser.api;

import com.nf.pg.converter.entities.UnitValue;
import com.nf.pg.converter.exceptions.UnitValueParsingException;

import javax.annotation.Nonnull;

/**
 * Defines unit value parser behavior.
 *
 * @see UnitValue
 */
public interface UnitValueParser {

    /**
     * Perform parsing of unit value from text line.
     *
     * @param value Text line in special format.
     * @return Parsed unit value data.
     * @see UnitValue
     * @throws UnitValueParsingException If specified text line has wrong format.
     */
    @Nonnull
    UnitValue parse(@Nonnull String value) throws UnitValueParsingException;
}
