package com.nf.pg.converter.parser.api;

import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.exceptions.UnitAssignmentParsingException;

import javax.annotation.Nonnull;

/**
 * Defines global assignment data parser.
 */
public interface AssignmentTableParser {

    /**
     * Perform parsing of assignment data from text.
     *
     * @param data Text to be parsed.
     * @return Parsed assignments.
     * @see AssignmentTable
     * @throws UnitAssignmentParsingException If specified text has unsupportable format.
     */
    @Nonnull
    AssignmentTable parse(@Nonnull String data) throws UnitAssignmentParsingException;
}
