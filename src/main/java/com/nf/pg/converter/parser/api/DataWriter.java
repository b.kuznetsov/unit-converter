package com.nf.pg.converter.parser.api;

import com.nf.pg.converter.entities.AssignmentTable;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * Defines a system able to write data somewhere.
 */
public interface DataWriter {

    /**
     * Writes assignment data somewhere (depends on realization).
     *
     * @param data Data for writing.
     * @throws IOException If something went wrong during the process of writing.
     */
    void write(@Nonnull AssignmentTable data) throws IOException;
}
