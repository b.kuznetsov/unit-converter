package com.nf.pg.converter.parser.implementation;

import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.parser.api.DataWriter;

import javax.annotation.Nonnull;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Writes results to a text file.
 */
public class FileDataWriter implements DataWriter {

    private String path;

    public FileDataWriter(@Nonnull String path) {
        this.path = path;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void write(@Nonnull AssignmentTable data) throws IOException {
        FileOutputStream fos = new FileOutputStream(path);
        String content = data.toString();
        fos.write(content.getBytes("UTF-8"));
    }
}
