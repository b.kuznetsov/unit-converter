package com.nf.pg.converter;

import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.exceptions.UnitConverterOperationException;
import com.nf.pg.converter.parser.api.DataProvider;
import com.nf.pg.converter.parser.api.DataWriter;

import javax.annotation.Nonnull;

/**
 * Defines basic behavior of the unit converter.
 */
public interface UnitConverter {

    /**
     * Loads data by specified data provider.
     * @param dataProvider Data provider.
     * @throws UnitConverterOperationException If something went wrong during reading from the data provider.
     */
    void load(@Nonnull DataProvider dataProvider) throws UnitConverterOperationException;

    /**
     * Performs conversion operation.
     *
     * @param data Converting data.
     * @throws UnitConverterOperationException If something went wrong during the conversion process.
     */
    void convert(@Nonnull AssignmentTable data) throws UnitConverterOperationException;

    /**
     * Registers conversion result output writers.
     *
     * @param writers Output writers.
     */
    void registerResultWriters(@Nonnull DataWriter... writers);

}
