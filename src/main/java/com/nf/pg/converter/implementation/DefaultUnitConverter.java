package com.nf.pg.converter.implementation;

import com.nf.pg.converter.UnitConverter;
import com.nf.pg.converter.entities.AssignmentTable;
import com.nf.pg.converter.exceptions.UnitAssignmentParsingException;
import com.nf.pg.converter.exceptions.UnitConverterOperationException;
import com.nf.pg.converter.parser.api.AssignmentTableParser;
import com.nf.pg.converter.parser.api.DataProvider;
import com.nf.pg.converter.parser.api.DataWriter;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DefaultUnitConverter implements UnitConverter {

    @Inject
    private AssignmentTableParser parser;

    private Set<DataWriter> resultWriters;

    public DefaultUnitConverter() {
        resultWriters = new HashSet<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void load(@Nonnull DataProvider dataProvider) throws UnitConverterOperationException {
        InputStream inputStream = tryLoadData(dataProvider);
        String content = read(inputStream);
        AssignmentTable parsingResult;
        try {
            parsingResult = parser.parse(content);
        } catch (UnitAssignmentParsingException e) {
            throw new UnitConverterOperationException("Cannot parse data", e);
        }

        convert(parsingResult);
    }

    private InputStream tryLoadData(@Nonnull DataProvider dataProvider) throws UnitConverterOperationException {
        try {
            return dataProvider.getData();
        } catch (IOException e) {
            throw new UnitConverterOperationException("Cannot load data from data provider", e);
        }
    }

    @Nonnull
    private String read(@Nonnull InputStream inputStream) throws UnitConverterOperationException {
        try {
            StringBuilder sb = new StringBuilder();
            int data;
            while ((data = inputStream.read()) != -1) {
                sb.append((char) data);
            }
            return sb.toString();
        } catch (IOException e) {
            throw new UnitConverterOperationException("Cannot read data from input stream");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert(@Nonnull AssignmentTable data) throws UnitConverterOperationException {
        data.calculate();
        writeResults(data);
    }

    private void writeResults(@Nonnull AssignmentTable data) {
        for (DataWriter writer : resultWriters) {
            try {
                writer.write(data);
            } catch (IOException e) {
                throw new RuntimeException("Cannot write results", e);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerResultWriters(@Nonnull DataWriter... writers) {
        resultWriters.addAll(Arrays.asList(writers));
    }
}
