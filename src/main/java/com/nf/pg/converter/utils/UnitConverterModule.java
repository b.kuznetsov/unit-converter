package com.nf.pg.converter.utils;

import com.google.inject.AbstractModule;
import com.nf.pg.converter.UnitConverter;
import com.nf.pg.converter.implementation.DefaultUnitConverter;
import com.nf.pg.converter.parser.api.AssignmentTableParser;
import com.nf.pg.converter.parser.api.UnitAssignmentParser;
import com.nf.pg.converter.parser.api.UnitValueParser;
import com.nf.pg.converter.parser.implementation.DefaultAssignmentTableParser;
import com.nf.pg.converter.parser.implementation.DefaultAssignmentParser;
import com.nf.pg.converter.parser.implementation.DefaultUnitValueParser;

/**
 * Guice DI module.
 */
public class UnitConverterModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(UnitConverter.class).to(DefaultUnitConverter.class);
        bind(UnitValueParser.class).to(DefaultUnitValueParser.class);
        bind(UnitAssignmentParser.class).to(DefaultAssignmentParser.class);
        bind(AssignmentTableParser.class).to(DefaultAssignmentTableParser.class);
    }
}
