package com.nf.pg.converter.entities;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents list of assignments.
 * @see UnitAssignment
 */
public final class AssignmentTable {

    private static final Logger log = LoggerFactory.getLogger(AssignmentTable.class);

    private Map<String, Map<String, Double>> table;
    private List<UnitAssignment> definitions;
    private List<UnitAssignment> undefined;

    public AssignmentTable() {
        this.table = new LinkedHashMap<>();
        this.definitions = new ArrayList<>();
        this.undefined = new ArrayList<>();
    }

    /**
     * Adds unit assignment to the table.
     *
     * @param assignment Units assignment.
     */
    public void addAssignment(@Nonnull UnitAssignment assignment) {
        if (assignment.getLeftSide().getValue().isPresent()
            && assignment.getRightSide().getValue().isPresent()) {
            definitions.add(assignment);
        } else {
            undefined.add(assignment);
        }
    }

    private void putRecord(UnitValue leftSide, UnitValue rightSide) {
        Map<String, Double> mapping = this.table.get(leftSide.getType());
        if (mapping == null) {
            mapping = new HashMap<>();
        }

        this.table.put(leftSide.getType(), mapping);
        Double ratio = rightSide.getValue().get() / leftSide.getValue().get();
        String subkey = rightSide.getType();
        if (!mapping.containsKey(subkey)) {
            mapping.put(subkey, ratio);
            putRecord(rightSide, leftSide);
        }
    }

    /**
     * Performs units conversion.
     */
    public void calculate() {
        fillTable();

        for (UnitAssignment undefinedAssignment : undefined) {
            Double value = searchValue(undefinedAssignment.getLeftSide(), undefinedAssignment.getRightSide());
            if (value != null) {
                undefinedAssignment.getRightSide().setValue(value);
                definitions.add(undefinedAssignment);
            } else {
                log.error("Cannot convert [{}] to [{}]",
                        undefinedAssignment.getLeftSide().getType(), undefinedAssignment.getRightSide().getType());
            }
        }
        undefined.removeIf(UnitAssignment::isAssigned);
    }

    @Nullable
    private Double searchValue(UnitValue targetValue, UnitValue resultValue) {
        Map<String, Double> mapping = table.get(targetValue.getType()); // 1 level
        if (mapping == null) {
            return null;
        }
        return searchInMapping(1d,  mapping, targetValue, resultValue);
    }

    @Nullable
    private Double searchInMapping(Double prevRatio, Map<String, Double> mapping,
                                   UnitValue targetValue, UnitValue resultValue) {

        for (String subKey : mapping.keySet()) {
            if (subKey.equals(targetValue.getType())) {
                continue;
            }

            if (subKey.equals(resultValue.getType())) {
                return targetValue.getValue().get() * prevRatio *  mapping.get(subKey);
            }
        }

        for (String subKey : mapping.keySet()) {
            if (subKey.equals(targetValue.getType())) {
                continue;
            }
            return searchInMapping(prevRatio * mapping.get(subKey), table.get(subKey),
                    targetValue, resultValue);
        }

        return null;
    }

    private void fillTable() {
        definitions.forEach(a -> putRecord(a.getLeftSide(), a.getRightSide()));
    }

    /**
     * Returns all assignments in the table.
     *
     * @return List of unit assignments.s
     */
    @Nonnull
    public List<UnitAssignment> getAssignments() {
        List<UnitAssignment> result = new ArrayList<>();
        result.addAll(definitions);
        result.addAll(undefined);
        return result;
    }

    @Override
    public String toString() {
        return getAssignments().stream()
                .map(UnitAssignment::toString)
                .collect(Collectors.joining(StringUtils.LF));
    }

}
