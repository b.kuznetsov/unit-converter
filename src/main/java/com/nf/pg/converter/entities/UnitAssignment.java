package com.nf.pg.converter.entities;

import javax.annotation.Nonnull;

/**
 * Unit assignment structure.<br>
 * Represents structure like <pre>[value1] [type1] = [value2] [type2]</pre>
 */
public final class UnitAssignment {

    private UnitValue leftSide;
    private UnitValue rightSide;

    /**
     * Constructs assignment structure with initialization by left and right side units.
     *
     * @param leftSide Left side unit value.
     * @param rightSide Right side unit value.
     */
    public UnitAssignment(@Nonnull UnitValue leftSide, @Nonnull UnitValue rightSide) {
        this.setLeftSide(leftSide);
        this.setRightSide(rightSide);
    }

    /**
     * Returns left side of the assignment.
     *
     * @return Unit value.
     */
    @Nonnull
    public UnitValue getLeftSide() {
        return leftSide;
    }

    /**
     * Sets left side of the assignment.
     *
     * @param leftSide Unit value.
     */
    public void setLeftSide(@Nonnull UnitValue leftSide) {
        this.leftSide = leftSide;
    }

    /**
     * Returns right side of the assignment.
     *
     * @return Unit value.
     */
    @Nonnull
    public UnitValue getRightSide() {
        return rightSide;
    }

    /**
     * Sets right side of the assignment.
     *
     * @param rightSide Unit value.
     */
    public void setRightSide(@Nonnull UnitValue rightSide) {
        this.rightSide = rightSide;
    }

    @Override
    public String toString() {
        return String.format("%s = %s", getLeftSide().toString(), getRightSide().toString());
    }

    /**
     * Returns true if it is correct assignment.
     *
     * @return True if left and right sides has values.
     */
    public boolean isAssigned() {
        return getLeftSide().getValue().isPresent() && getRightSide().getValue().isPresent();
    }
}
