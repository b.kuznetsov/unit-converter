package com.nf.pg.converter.entities;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Unit value structure.
 * Represents structure like: <pre>[value] [type]</pre>
 */
public final class UnitValue {

    private String type;
    private Optional<Double> value;

    /**
     * Constructs undefined value unit.
     *
     * @param type Type of unit.
     */
    public UnitValue(@Nonnull String type) {
        setType(type);
        setValue(null);
    }

    /**
     * Constructs unit with type and defined value.
     *
     * @param type Unit type.
     * @param value Unit value.
     */
    public UnitValue(@Nonnull String type, @Nonnull Double value) {
        setType(type);
        setValue(value);
    }

    /**
     * Returns type of the unit.
     *
     * @return Type name.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type of the unit.
     *
     * @param type Unit type name.
     */
    public void setType(@Nonnull String type) {
        this.type = type;
    }

    /**
     * Returns value of the unit.
     *
     * @return Value of the unit.
     */
    @Nonnull
    public Optional<Double> getValue() {
        return value;
    }

    /**
     * Sets value of the unit.
     *
     * @param value Value of the unit.
     */
    public void setValue(Double value) {
        this.value = Optional.ofNullable(value);
    }

    @Override
    public String toString() {
        String value = getValue().isPresent()
                ? getValue().get().toString()
                : "?";
        return String.format("%s %s", value, getType());
    }
}
