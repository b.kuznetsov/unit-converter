package com.nf.pg.converter.entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AssignmentTableTest {

    private AssignmentTable table;

    @Before
    public void before() {
        table = new AssignmentTable();
    }

    @Test
    public void test() {
        UnitValue a1L = new UnitValue("byte", 1d);
        UnitValue a1R = new UnitValue("bit", 8d);
        UnitAssignment a1 = new UnitAssignment(a1L, a1R);
        table.addAssignment(a1);

        UnitValue a2L = new UnitValue("byte", 1024d);
        UnitValue a2R = new UnitValue("kilobyte", 1d);
        UnitAssignment a2 = new UnitAssignment(a2L, a2R);
        table.addAssignment(a2);

        UnitValue a3L = new UnitValue("kilobyte", 1d);
        UnitValue a3R = new UnitValue("bit");
        UnitAssignment a3 = new UnitAssignment(a3L, a3R);
        table.addAssignment(a3);
        table.calculate();

        Assert.assertTrue(a3.getLeftSide().getValue().isPresent());
        Assert.assertTrue(a3.getRightSide().getValue().get() == 8192d);
    }

}